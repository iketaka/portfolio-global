
jQuery(function ($) { // この中であればWordpressでも「$」が使用可能になる

  var topBtn = $('.pagetop');
  topBtn.hide();

  // ボタンの表示設定
  $(window).scroll(function () {
    if ($(this).scrollTop() > 70) {
      // 指定px以上のスクロールでボタンを表示
      topBtn.fadeIn();
    } else {
      // 画面が指定pxより上ならボタンを非表示
      topBtn.fadeOut();
    }
  });

  // ボタンをクリックしたらスクロールして上に戻る
  topBtn.click(function () {
    $('body,html').animate({
      scrollTop: 0
    }, 300, 'swing');
    return false;
  });

  //ドロワーメニュー
  // $("#MenuButton").click(function () {
  //   // $(".l-drawer-menu").toggleClass("is-show");
  //   // $(".p-drawer-menu").toggleClass("is-show");
  //   $(".js-drawer-open").toggleClass("open");
  //   $(".drawer-menu").toggleClass("open");
  //   $("html").toggleClass("is-fixed");

  // });
  /* ドロワー */
  const drawerIcon = jQuery('#drawer-icon')
  const drawerContent = jQuery('#drawer-content')
  const drawerLink = jQuery('.drawer__link')
  const drawerCheckBox = jQuery('#drawer-checkbox')
  const drawerOpen = jQuery('.js-drawer-open')

  drawerIcon.on('click', function () {
    drawerContent.slideToggle(200)
    drawerOpen.toggleClass('is-fixed')
  })

  /* drawerナビを押した時にdrawerを元に戻す（ブラウザバック対応） */
  drawerLink.on('click', function () {
    drawerCheckBox.prop('checked', false)
    drawerContent.slideUp(200)
  })



  // スムーススクロール (絶対パスのリンク先が現在のページであった場合でも作動)

  $(document).on('click', 'a[href*="#"]', function () {
    let time = 400;
    let header = $('header').innerHeight();
    let target = $(this.hash);
    if (!target.length) return;
    let targetY = target.offset().top - header;
    $('html,body').animate({ scrollTop: targetY }, time, 'swing');
    return false;
  });

   /* トップページcase studyのホバーイベント */
const handleHoverIn = function() {
  jQuery(this).addClass("is-hover");
};
const handleHoverOut = function() {
  jQuery(this).removeClass("is-hover");
};
jQuery(".js-case-hover").hover(handleHoverIn, handleHoverOut);


function adjustHeight(){
  let baseHeight = jQuery(".js-base-height").outerHeight();
  let targetHeight = jQuery(".js-target-height");
  const width = jQuery(window).width();
  if(width >= 768){
    targetHeight.outerHeight(baseHeight);
  } else {
    targetHeight.css("height", "auto");
  }
}
adjustHeight();
jQuery(window).on("resize", function() {
  adjustHeight();
});

  // スライダー
  const swiper = new Swiper('.swiper-container', {
    loop: true,
    speed: 1000,
    autoplay: {
      delay: 3000,
    },
    effect: 'slide',
    slidesPerView: 1

  })

  /* アコーディオン */
  const accClick = jQuery('.js-accordion')
  accClick.on('click', function () {
    /*クリックでコンテンツを開閉*/
    jQuery(this).next().slideToggle(200)
    /*矢印の向きを変更*/
    jQuery(this).toggleClass('is-open', 200)
  })

  /* ここから */

  const breakpoint = 640
  const mql = window.matchMedia(`screen and (max-width: ${breakpoint}px)`) //、MediaQueryListの生成
  let deviceFlag = mql.matches ? 1 : 0 // 0 : PC ,  1 : SP

  /* ページトップへ戻る */
  const pageTop = jQuery("#js-pagetop");
  const speed = 300;
  pageTop.on("click", function() {
    jQuery("body, html").animate({
      scrollTop: 0
    }, speed);
    return false;
  });

  // スムーズスクロール
  // #で始まるアンカーをクリックした場合にスムーススクロール
  $('a[href^="#"]').on('click', function () {
    const speed = 500
    // アンカーの値取得
    const href = $(this).attr('href')
    // 移動先を取得
    const target = $(href == '#' || href == '' ? 'html' : href)
    // 移動先を数値で取得
    const position = target.offset().top

    // スムーススクロール
    $('body,html').animate(
      {
        scrollTop: position,
      },
      speed,
      'swing',
    )
    return false
  })

// オブジェクトフィット
/* polyfill対応 */
objectFitImages();
// picturefill();

});
