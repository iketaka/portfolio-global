<?php get_header(); ?>

<div class="404">
  お探しのページは見つかりませんでした（404）<br>
  大変申し訳ありません。お探しのページは移動又は削除されているようです。
</div>
<div class="404-link">
  <a href="http://global1.local/">トップページへ戻る</a>
</div>

<?php get_footer(); ?>
