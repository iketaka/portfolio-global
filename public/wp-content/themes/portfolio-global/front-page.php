<?php get_header(); ?>

<section class="mv">
      <div class="mv__inner">
        <div class="mv-slider swiper-container">
          <!-- 新しいswiper -->
          <div class="mv-slider__items swiper-wrapper">
            <!-- <div class="swiper-wrapper"> -->
            <div class="mv-slider__item swiper-slide">
              <picture>
                <source
                  srcset="<?php echo get_template_directory_uri(  ); ?>/images/common/mv.jpg"
                  media="(min-width: 768px)"
                />
                <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-mv01_sp.png" alt="会議中の画像" />
              </picture>
            </div>
            <div class="mv-slider__item swiper-slide">
              <picture>
                <source
                  srcset="<?php echo get_template_directory_uri(  ); ?>/images/common/mv2.jpg"
                  media="(min-width: 768px)"
                />
                <img
                  src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-mv02_sp.png"
                  alt="運動をしている画像"
                />
              </picture>
            </div>
            <div class="mv-slider__item swiper-slide">
              <picture>
                <source
                  srcset="<?php echo get_template_directory_uri(  ); ?>/images/common/mv3.jpg"
                  media="(min-width: 768px)"
                />
                <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-mv03_sp.png" alt="会話中の画像" />
              </picture>
            </div>
            <!-- </div> -->
          </div>
        </div>
        <!-- 新しいswiper終了 -->
        <!-- mv-text -->
        <div class="mv-slider__main">
            <h2 class="mv-slider__title-wrapper">
              <div class="mv-slider__title-above">
                <p class="mv-slider__title">YOU CAN</p>
                <p class="mv-slider__title mv-slider__title--spacing">
                  CHANGE
                </p>
              </div>
              <p class="mv-slider__title">THE WORLD</p>
            </h2>
            <p class="mv-slider__text">
              世界で活躍できるグローバルな人材を育てる
            </p>
          </div>
          <!-- mv-text fin -->
      </div>
    </section>
    <!-- mv終了 -->
    <!-- about開始-->
    <section class="company section skew">
      <div class="company__content">
        <div class="company__bg company__content-bg"></div>
      </div>
      <div class="inner">
        <!-- ヘッド開始 -->
        <div class="heading">
          <div class="heading__main">
            <h2 class="heading__text--en u-font-italic">ABOUT US</h2>
            <p class="heading__text--ja">当社について</p>
          </div>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>/company" class="heading__link u-color-black u-hidden-sp">
            <span class="heading__link-text heading__link-text--white u-font-italic">View more</span>
            <div class="heading__link-arrow--color-white"></div>
            <div class="heading__link-circle--border"></div>
          </a>
        </div>
        <div class="company__items">
          <p class="company__text">
            急速に広がったグローバル社会に対応できる人材を育成することで、文化・言語の垣根を越えたコミュニケーションを活発にし、一人でも多くの人が豊かに暮らせる世界を実現することを使命とする。
            <br />
            <br />
            コミュニケーションスキル習得をサポートすることで一人でも多くのビジネスパーソンの視野を広げ、世界を舞台に新しい相乗効果を生む未来を創造する。
            <br />
            <br />
            文化の垣根を越えた人と人とのつながりが新しい価値を生むことを信念とする。
          </p>
          <div class="company__img-wrapper">
            <figure class="company__img">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-company.png" alt="" />
            </figure>
            <a
              href=""
              class="heading__link company__view-more-link u-color-white u-hidden-pc"
            >
              <span class="heading__link-text u-font-italic">View more</span>
              <div class="heading__link-arrow--color-white"></div>
              <div class="heading__link-circle--border"></div>
            </a>
            <div class="company__bg company__img-bg"></div>
          </div>
        </div>
      </div>
    </section>
    <!-- about終了 -->
    <!-- service開始 -->
    <section class="service section skew">
      <div class="inner">
        <!-- ヘッド -->
        <div class="heading">
          <div class="heading__main">
            <h2 class="heading__text--en u-font-italic">SERVICE</h2>
            <p class="heading__text--ja">サービス</p>
          </div>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>/service" class="heading__link u-color-black u-hidden-sp">
            <span class="heading__link-text u-font-italic">View more</span>
            <div class="heading__link-arrow--color-black"></div>
            <div class="heading__link-circle--fill"></div>
          </a>
        </div>
        <!-- ヘッド終了 -->
        <!-- サービスアイテム -->
        <div class="service__items">
          <div class="service__item">
            <div class="service__item-img">
              <div class="service__item-number">
                <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/number01.png" alt="数字の画像1" />
              </div>
              <img
                src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-service01.png"
                alt="打ち合わせの画像"
              />
              <h3 class="service__item-title">
                <span class="service__item-title--inside">
                  ビジネス英語研修
                </span>
              </h3>
            </div>
            <p class="service__item-text">
              ビジネス英会話はこれからの時代、すべてのビジネスパーソンが学ぶべき必須スキルと考えおります。海外にビジネス展開する際にはもちろんのこと、日本国内でも英会話コミュニケーションができることによってチャンスが掴める場面があります。
            </p>
          </div>
          <div class="service__item">
            <div class="service__item-img">
              <div class="service__item-number">
                <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/number02.png" alt="数字の画像2" />
              </div>
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-service02.png" alt="原住民の画像" />
              <h3 class="service__item-title">
                <span class="service__item-title--inside">異文化</span>
                <br />
                <span class="service__item-title--inside">
                  コミュニケーション研修
                </span>
              </h3>
            </div>
            <p class="service__item-text">
              急速にグローバル化が進んでおり、ビジネスの場面に限らず様々な文化的背景を持つ者同士の交流はもはや日常的な光景となりました。
              <br />
              言語や文化が異なる相手を理解することで世界が広がり、新たなビジネスチャンスに巡り会うことは少なくありません。
            </p>
          </div>
          <div class="service__item">
            <div class="service__item-img">
              <div class="service__item-number">
                <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/number03.png" alt="数字の画像3" />
              </div>
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-service03.png" alt="討論中の画像" />
              <h3 class="service__item-title">
                <span class="service__item-title--inside">ビジネス</span>
                <br />
                <span class="service__item-title--inside">
                  サポートプログラム
                </span>
              </h3>
            </div>
            <p class="service__item-text">
              将来的に海外で働きたい方に向けた講座をご用意しております。一般的には3ヶ月〜1年の期間Ïで基本的な英会話スキルと、海外でのビジネスマナー習得を目指します。
              <br />
              通常の語学留学では得られないビジネスの場で通用するコミュニケーションスキル習得に重点をおいておりますので、海外でビジネス展開する際に自信を持って活動することができるようになります。
            </p>
          </div>
        </div>
        <!-- サービスアイテム終了 -->
        <!-- スマホ時heading開始 -->
        <a
          href="<?php echo esc_url( home_url( '/' ) ); ?>/service"
          class="heading__link service__view-more-link u-color-blue u-hidden-pc"
        >
          <span class="heading__link-text u-font-italic">View more</span>
          <div class="heading__link-arrow--color-blue"></div>
          <div class="heading__link-circle--fill"></div>
        </a>
        <!-- スマホ時heading終了 -->
      </div>
    </section>
    <!-- service終了 -->
    <!-- case開始 -->
    <section class="case section">
      <div class="inner">
        <!-- ヘッド開始 -->
        <div class="case__heading">
          <div class="heading">
            <div class="heading__main">
              <h2 class="heading__text--en--white u-font-italic">
                CASE STUDY
              </h2>
              <p class="heading__text--ja--white">導入事例</p>
            </div>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>/case" class="heading__link u-color-black u-hidden-sp">
              <span
                class="heading__link-text heading__link-text--white u-font-italic"
              >
                View more
              </span>
              <div class="heading__link-arrow--color-white"></div>
              <div class="heading__link-circle--border"></div>
            </a>
          </div>
        </div>
        <!-- ヘッド終了 -->
        <div class="case__items">
          <!-- case item -->
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>/case#AAA" class="case__item js-case-hover">
            <div class="case__item-img">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-1.png" alt="logo" />
            </div>
            <div class="case__item-company">AAA株式会社 様</div>
            <div class="case__item--links">
              <div class="case__item-cat">ビジネス英語研修</div>
              <div class="case__item-link">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="29.25"
                  height="29.25"
                  viewBox="0 0 29.25 29.25"
                >
                  <g id="arrow-icon" transform="translate(-3.375 -3.375)">
                    <path
                      id="パス_4104"
                      data-name="パス 4104"
                      d="M14.815,10.378a1.362,1.362,0,0,1,1.92,0l6.708,6.729a1.356,1.356,0,0,1,.042,1.87l-6.609,6.63a1.355,1.355,0,1,1-1.92-1.912l5.618-5.7-5.759-5.7A1.341,1.341,0,0,1,14.815,10.378Z"
                    />
                    <path
                      id="パス_4105"
                      data-name="パス 4105"
                      d="M3.375,18A14.625,14.625,0,1,0,18,3.375,14.623,14.623,0,0,0,3.375,18Zm2.25,0A12.37,12.37,0,0,1,26.747,9.253,12.37,12.37,0,1,1,9.253,26.747,12.269,12.269,0,0,1,5.625,18Z"
                    />
                  </g>
                </svg>
              </div>
            </div>
          </a>
          <!-- case item -->
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>/case#BBB" class="case__item js-case-hover">
            <div class="case__item-img">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-5.png" alt="logo" />
            </div>
            <div class="case__item-company">合同会社BBB　様</div>
            <div class="case__item--links">
              <div class="case__item-cat">異文化コミュニケーション</div>
              <div class="case__item-link">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="29.25"
                  height="29.25"
                  viewBox="0 0 29.25 29.25"
                >
                  <g id="arrow-icon" transform="translate(-3.375 -3.375)">
                    <path
                      id="パス_4104"
                      data-name="パス 4104"
                      d="M14.815,10.378a1.362,1.362,0,0,1,1.92,0l6.708,6.729a1.356,1.356,0,0,1,.042,1.87l-6.609,6.63a1.355,1.355,0,1,1-1.92-1.912l5.618-5.7-5.759-5.7A1.341,1.341,0,0,1,14.815,10.378Z"
                    />
                    <path
                      id="パス_4105"
                      data-name="パス 4105"
                      d="M3.375,18A14.625,14.625,0,1,0,18,3.375,14.623,14.623,0,0,0,3.375,18Zm2.25,0A12.37,12.37,0,0,1,26.747,9.253,12.37,12.37,0,1,1,9.253,26.747,12.269,12.269,0,0,1,5.625,18Z"
                    />
                  </g>
                </svg>
              </div>
            </div>
          </a>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>/case#CCC" class="case__item js-case-hover">
            <div class="case__item-img">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case08.png" alt="logo" />
            </div>
            <div class="case__item-company">株式会社CCC　様</div>
            <div class="case__item--links">
              <div class="case__item-cat">ビジネス留学プログラム</div>
              <div class="case__item-link">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="29.25"
                  height="29.25"
                  viewBox="0 0 29.25 29.25"
                >
                  <g id="arrow-icon" transform="translate(-3.375 -3.375)">
                    <path
                      id="パス_4104"
                      data-name="パス 4104"
                      d="M14.815,10.378a1.362,1.362,0,0,1,1.92,0l6.708,6.729a1.356,1.356,0,0,1,.042,1.87l-6.609,6.63a1.355,1.355,0,1,1-1.92-1.912l5.618-5.7-5.759-5.7A1.341,1.341,0,0,1,14.815,10.378Z"
                    />
                    <path
                      id="パス_4105"
                      data-name="パス 4105"
                      d="M3.375,18A14.625,14.625,0,1,0,18,3.375,14.623,14.623,0,0,0,3.375,18Zm2.25,0A12.37,12.37,0,0,1,26.747,9.253,12.37,12.37,0,1,1,9.253,26.747,12.269,12.269,0,0,1,5.625,18Z"
                    />
                  </g>
                </svg>
              </div>
            </div>
          </a>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>/case#DDD" class="case__item js-case-hover">
            <div class="case__item-img">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-6.png" alt="logo" />
            </div>
            <div class="case__item-company">DDD株式会社　様</div>
            <div class="case__item--links">
              <div class="case__item-cat">異文化コミュニケーション</div>
              <div class="case__item-link">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="29.25"
                  height="29.25"
                  viewBox="0 0 29.25 29.25"
                >
                  <g id="arrow-icon" transform="translate(-3.375 -3.375)">
                    <path
                      id="パス_4104"
                      data-name="パス 4104"
                      d="M14.815,10.378a1.362,1.362,0,0,1,1.92,0l6.708,6.729a1.356,1.356,0,0,1,.042,1.87l-6.609,6.63a1.355,1.355,0,1,1-1.92-1.912l5.618-5.7-5.759-5.7A1.341,1.341,0,0,1,14.815,10.378Z"
                    />
                    <path
                      id="パス_4105"
                      data-name="パス 4105"
                      d="M3.375,18A14.625,14.625,0,1,0,18,3.375,14.623,14.623,0,0,0,3.375,18Zm2.25,0A12.37,12.37,0,0,1,26.747,9.253,12.37,12.37,0,1,1,9.253,26.747,12.269,12.269,0,0,1,5.625,18Z"
                    />
                  </g>
                </svg>
              </div>
            </div>
          </a>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>/case#EEE" class="case__item js-case-hover">
            <div class="case__item-img">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case09.png" alt="logo" />
            </div>
            <div class="case__item-company">EEE株式会社　様</div>
            <div class="case__item--links">
              <div class="case__item-cat">ビジネス留学プログラム</div>
              <div class="case__item-link">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="29.25"
                  height="29.25"
                  viewBox="0 0 29.25 29.25"
                >
                  <g id="arrow-icon" transform="translate(-3.375 -3.375)">
                    <path
                      id="パス_4104"
                      data-name="パス 4104"
                      d="M14.815,10.378a1.362,1.362,0,0,1,1.92,0l6.708,6.729a1.356,1.356,0,0,1,.042,1.87l-6.609,6.63a1.355,1.355,0,1,1-1.92-1.912l5.618-5.7-5.759-5.7A1.341,1.341,0,0,1,14.815,10.378Z"
                    />
                    <path
                      id="パス_4105"
                      data-name="パス 4105"
                      d="M3.375,18A14.625,14.625,0,1,0,18,3.375,14.623,14.623,0,0,0,3.375,18Zm2.25,0A12.37,12.37,0,0,1,26.747,9.253,12.37,12.37,0,1,1,9.253,26.747,12.269,12.269,0,0,1,5.625,18Z"
                    />
                  </g>
                </svg>
              </div>
            </div>
          </a>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>/case#FFF" class="case__item js-case-hover">
            <div class="case__item-img">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-2.png" alt="logo" />
            </div>
            <div class="case__item-company">FFF株式会社　様</div>
            <div class="case__item--links">
              <div class="case__item-cat">ビジネス英語研修</div>
              <div class="case__item-link">
                <svg
                  xmlns="ihttp://www.w3.org/2000/svg"
                  width="29.25"
                  height="29.25"
                  viewBox="0 0 29.25 29.25"
                >
                  <g id="arrow-icon" transform="translate(-3.375 -3.375)">
                    <path
                      id="パス_4104"
                      data-name="パス 4104"
                      d="M14.815,10.378a1.362,1.362,0,0,1,1.92,0l6.708,6.729a1.356,1.356,0,0,1,.042,1.87l-6.609,6.63a1.355,1.355,0,1,1-1.92-1.912l5.618-5.7-5.759-5.7A1.341,1.341,0,0,1,14.815,10.378Z"
                    />
                    <path
                      id="パス_4105"
                      data-name="パス 4105"
                      d="M3.375,18A14.625,14.625,0,1,0,18,3.375,14.623,14.623,0,0,0,3.375,18Zm2.25,0A12.37,12.37,0,0,1,26.747,9.253,12.37,12.37,0,1,1,9.253,26.747,12.269,12.269,0,0,1,5.625,18Z"
                    />
                  </g>
                </svg>
              </div>
            </div>
          </a>
        </div>
        <a
          href="<?php echo esc_url( home_url( '/' ) ); ?>/case"
          class="heading__link company__view-more-link u-color-white u-hidden-pc"
        >
          <span class="heading__link-text u-font-italic">View more</span>
          <div class="heading__link-arrow--color-white"></div>
          <div class="heading__link-circle--border"></div>
        </a>
      </div>
    </section>
    <!-- case終了 -->
    <!-- News開始 -->
    <section class="news section skew">
      <div class="inner">
        <!-- ヘッド開始 -->
        <div class="case__heading">
          <div class="heading">
            <div class="heading__main">
              <h2 class="heading__text--en u-font-italic">NEWS</h2>
              <p class="heading__text--ja">新着情報</p>
            </div>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>/news" class="heading__link u-color-black u-hidden-sp">
              <span class="heading__link-text u-font-italic">View more</span>
              <div class="heading__link-arrow--color-black"></div>
              <div class="heading__link-circle--fill"></div>
            </a>
          </div>
        </div>
        <!-- ヘッド終了 -->
      </div>
      <!-- news item -->
      <div class="inner--narrow">
        <div class="news__items">
        <?php
        $args = array( 'posts_per_page' => 3, );  
        $postslist = get_posts( $args );

        foreach ( $postslist as $post ) :
        setup_postdata( $post ); ?> 
          <a href="<?php the_permalink(); ?>" class="news__item">
            <div class="news__info">
            <?php
            // カテゴリー１つ目の名前を表示
            $category = get_the_category();
            if ($category[0] ) {
            echo '<span class="news__info-cat">' . $category[0]->cat_name . '</span>';
            }
            ?>   
              <time class="news__info-time" datetime="<?php the_time('c'); ?>"><?php the_time('Y/n/j'); ?></time>
            </div>
            <h3 class="news__title"><?php the_title(); ?></h3>
          </a>
          
          <?php
          endforeach;
          wp_reset_postdata();
          ?>
        <!-- スマホ時heading開始 -->
        <a
          href="<?php echo esc_url( home_url( '/' ) ); ?>/news"
          class="heading__link news__view-more-link u-color-blue u-hidden-pc"
        >
          <span class="heading__link-text u-font-italic">View more</span>
          <div class="heading__link-arrow--color-blue"></div>
          <div class="heading__link-circle--fill"></div>
        </a>
        <!-- スマホ時heading終了 -->
      </div>
    </section>
    <!-- news終了 -->


<?php get_footer(); ?>