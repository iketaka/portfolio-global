<?php get_header(); ?>

<!-- 下層ページmv -->
<div class="mv-lower">
  <div class="mv-lower__img--company">
      </div>
      <div class="mv-lower__main">
        <div class="mv-lower__en-wrapper">
          <h2 class="mv-lower__en u-font-italic">ABOUT US</h2>
        </div>
        <div class="mv-lower__ja-wrapper">
          <p class="mv-lower__ja">当社について</p>
        </div>
      </div>
    </div>
    <!-- 下層ページmv終了 -->
<!-- パンクズ開始 -->
<div class="breadcrumbs">
  <div class="inner">
    <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
      <?php bcn_display(); //BreadcrumbNavXTのパンくずを表示するための記述 ?>
    </ol>
  </div>
</div>
<!-- パンクズ終了 -->
    <!-- カンパニーメッセージ開始 -->
    <section class="company-message">
      <div class="inner--wide">
        <div class="company-message__item">
          <div class="company-message__bg--bg01">
            <h2 class="company-message__bg-en u-font-italic">MISSION</h2>
            <p class="company-message__bg-jp">社会的使命</p>
          </div>
          <div class="company-message__box">
            <h3 class="company-message__box-title">人材育成を通じて、
              <br class="u-hidden-pc">
              豊かな世界を創造する
            </h3>
            <p class="company-message__box-text">
              急速に広がったグローバル社会に対応できる人材を育成することで、文化・言語の垣根を越えたコミュニケーションを活発にし、一人でも多くの人が豊かに暮らせる世界を実現することを使命とする。
            </p>
          </div>
        </div>
        <div class="company-message__item">
          <div class="company-message__bg--bg02">
            <h2 class="company-message__bg-en u-font-italic">VISION</h2>
            <p class="company-message__bg-jp">企業理念</p>
          </div>
          <div class="company-message__box">
            <h3 class="company-message__box-title">文化の垣根を越えた    
              <br class="u-hidden-sp">
              人と人とのつながりが新しい価値を生む
            </h3>
            <p class="company-message__box-text">
              コミュニケーションスキル習得をサポートすることで一人でも多くのビジネスパーソンの視野を広げ、世界を舞台に新しい相乗効果を生む未来を創造する。文化の垣根を越えた人と人とのつながりが新しい価値を生むことを信念とする。
            </p>
          </div>
        </div>
      </div>
    </section>
    <!-- カンパニーメッセージ終了 -->
    <!-- 企業概要開始 -->
    <section class="company-profile">
      <div class="inner--narrow">
        <div class="company-profile__box">
          <h2 class="company-profile__title">会社概要</h2>
          <dl class="company-profile__dl">
            <dt class="company-profile__dt">代表</dt>
            <dd class="company-profile__dd">波瑠&emsp;慶太</dd>
            <dt class="company-profile__dt">事業内容</dt>
            <dd class="company-profile__dd">
              <ul class="company-profile__dd-lists">
                <li class="company-profile__dd-list">
                  ビジネス英会話教育事業
                </li>
                <li class="company-profile__dd-list">
                  異文化交流サポート事業
                </li>
                <li class="company-profile__dd-list">
                  ビジネス留学事業
                </li>
              </ul>
            </dd>
            <dt class="company-profile__dt">設立</dt>
            <dd class="company-profile__dd">2012年10月03日</dd>
            <dt class="company-profile__dt">所在地</dt>
            <dd class="company-profile__dd">〒550-1000
              <br>
              大阪市西区土佐堀9-5-5
            </dd>
            <dt class="company-profile__dt">TEL</dt>
            <dd class="company-profile__dd">06-123-4567（代表）</dd>
            <dt class="company-profile__dt">FAX</dt>
            <dd class="company-profile__dd">06-123-4568（代表）</dd>
            <dt class="company-profile__dt">E-mail</dt>
            <dd class="company-profile__dd">globalstandard@example.com</dd>
          </dl>
        </div>
      </div>
    </section>
    <!-- 企業概要終了 -->
    <!-- 会社役員紹介開始 -->
    <section class="company-directors section skew">
      <div class="inner">
        <h2 class="company-directors__title">役員紹介</h2>
        <div class="company-directors__items">
        <div class="company-directors__item">
          <figure class="company-directors__img">
            <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-directors01.png" alt="代表取締役">
          </figure>
          <div class="company-directors__main">
            <div class="company-directors__profile">
              <p class="company-directors__job-title">代表取締役</p>
              <h3 class="company-directors__name">波瑠&emsp;慶太</h3>
            </div>
            <p class="company-directors__biography">
            20年間外資系企業に勤務し、世界17カ国でビジネスを展開。<br>
            様々な文化に触れ、コミュニケーションスキルを磨き、同時にその必要性を実感する。<br>
            自身も講師を務め、実体験から得られた知見を皆様に還元し、グローバルなビジネス展開をサポートいたします。
            </p>
            <div class="company-directors__sns-icons">
              <a href="" class="company-directors__sns-icon"><img src="<?php echo get_template_directory_uri(  ); ?>/images/common/icon-twitter.png" alt="twitter"></a>
              <a href="" class="company-directors__sns-icon"><img src="<?php echo get_template_directory_uri(  ); ?>/images/common/icon-facebook.png" alt="facebook"></a>
              <a href="" class="company-directors__sns-icon"><img src="<?php echo get_template_directory_uri(  ); ?>/images/common/icon-instagram.png" alt="instagram"></a>
            </div>
        </div>
      </div>
        <div class="company-directors__item">
          <figure class="company-directors__img">
            <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-directors02.png" alt="取締役">
          </figure>
          <div class="company-directors__main">
            <div class="company-directors__profile">
              <p class="company-directors__job-title">取締役</p>
              <h3 class="company-directors__name">ジャック・スミス</h3>
            </div>
            <p class="company-directors__biography">
            オーストラリア出身。
            <br>
            英会話の講師として13年のキャリアがあります。<br>
            翻訳業務も担当しており、外国映画の日本版DVDの字幕やテレビ番組の英語をヒヤリングなども行なっております
            <br>
            皆様に「より気持ちの伝わる英会話」を習得していただくサポートをいたします。
            </p>
            <div class="company-directors__sns-icons">
              <a href="" class="company-directors__sns-icon"><img src="<?php echo get_template_directory_uri(  ); ?>/images/common/icon-twitter.png" alt="twitter"></a>
              <a href="" class="company-directors__sns-icon"><img src="<?php echo get_template_directory_uri(  ); ?>/images/common/icon-facebook.png" alt="facebook"></a>
              <a href="" class="company-directors__sns-icon"><img src="<?php echo get_template_directory_uri(  ); ?>/images/common/icon-instagram.png" alt="instagram"></a>
            </div>
        </div>
      </div>
        <div class="company-directors__item">
          <figure class="company-directors__img">
            <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-directors03.png" alt="取締役">
          </figure>
          <div class="company-directors__main">
            <div class="company-directors__profile">
              <p class="company-directors__job-title">取締役</p>
              <h3 class="company-directors__name">メアリー・ジャクソン</h3>
          </div>
          <p class="company-directors__biography">
            アメリカ出身。
            <br>
            メジャーリーグ球団「ニューヨークヤンキース」の通訳担当として7年間チームに在籍。<br>
            数多くの契約交渉の経験を活かし、国際ビジネスにおけるコミュニケーションのマナーから応用までお伝えいたします。
          </p>
          <div class="company-directors__sns-icons">
            <a href="" class="company-directors__sns-icon"><img src="<?php echo get_template_directory_uri(  ); ?>/images/common/icon-twitter.png" alt="twitter"></a>
            <a href="" class="company-directors__sns-icon"><img src="<?php echo get_template_directory_uri(  ); ?>/images/common/icon-facebook.png" alt="facebook"></a>
            <a href="" class="company-directors__sns-icon"><img src="<?php echo get_template_directory_uri(  ); ?>/images/common/icon-instagram.png" alt="instagram"></a>
          </div>
        </div>
      </div>
    </div>
    </section>
    <!-- 会社役員紹介終了 -->


<?php get_footer(); ?>