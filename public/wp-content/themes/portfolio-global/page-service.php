<?php get_header(); ?>

<!-- 下層ページmv -->
<div class="mv-lower">
      <div class="mv-lower__img--service"></div>
      <div class="mv-lower__main">
        <div class="mv-lower__en-wrapper">
          <h2 class="mv-lower__en u-font-italic">SERVICE</h2>
        </div>
        <div class="mv-lower__ja-wrapper">
          <p class="mv-lower__ja">サービス</p>
        </div>
      </div>
    </div>
    <!-- 下層ページmv終了 -->
    <!-- パンクズ開始 -->
<div class="breadcrumbs">
  <div class="inner">
    <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
      <?php bcn_display(); //BreadcrumbNavXTのパンくずを表示するための記述 ?>
    </ol>
  </div>
</div>
<!-- パンクズ終了 -->
    <!-- service-detail開始 -->
    <section class="service-detail section--lower-fv">
      <div class="inner">
        <p class="service-detail__intro">
          世界で活躍できる
          <br />
          グローバルな人材を育てる
          <br class="u-hidden-pc" />
          ３つの研修プログラム
        </p>
      </div>
      <article class="service-detail__items">
        <div class="service-detail__inner">
          <div id="case1" class="service-detail__item service-detail__anchor">
            <figure class="service-detail__img">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-service-detail01.png" alt="ミーティング画像" />
            </figure>
            <div class="service-detail__main">
              <span class="service-detail__number u-font-italic">01</span>
              <div class="service-detail__heding">
                <h2 class="service-detail__jp">ビジネス英語研修</h2>
                <p class="service-detail__en u-font-italic">
                  Business English Training
                </p>
              </div>
              <p class="service-detail__text">
                ビジネス英会話はこれからの時代、すべてのビジネスパーソンが学ぶべき必須スキルと考えおります。
                <br />
                海外にビジネス展開する際にはもちろんのこと、日本国内でも英会話コミュニケーションができることによってチャンスが掴める場面があります。
                <br />
                <br />
                担当する講師は皆、豊富な海外ビジネス経験者であり、ティーチングスキル、コミュニケーションスキル、人間性に加えて採用後には厳しいトレーニング期間を設けているので、様々な職業や職種に合ったスキルまで身につけられます。
                <br />
                また、必要に応じてマンツーマン形式のレッスンを行うことも可能なので、時間の限り話すことができ効率よく上達することができます。
              </p>
              <div class="service-detail__dl-wrapper">
                <dl class="service-detail__dl">
                  <dt class="service-detail__dt">対象</dt>
                  <dd class="service-detail__dd">
                    ビジネスの中で使える英語コニュニケーション能力が必要な方
                  </dd>
                </dl>
                <dl class="service-detail__dl">
                  <dt class="service-detail__dt">費用</dt>
                  <dd class="service-detail__dd">時間内容要相談</dd>
                </dl>
              </div>
              <a
                href="<?php echo esc_url( home_url( '/' ) ); ?>/contact"
                class="service-detail__button btn--primary btn--icon-arrow"
              >
                お申し込みはこちら
              </a>
            </div>
          </div>
        </div>
      </article>
      <article class="service-detail__items skew">
        <div class="service-detail__inner">
          <div id="case2" class="service-detail__item service-detail__anchor">
            <figure class="service-detail__img">
              <img
                src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-service-detail02.png"
                alt="原住民の画像"
              />
            </figure>
            <div class="service-detail__main">
              <span class="service-detail__number u-font-italic">02</span>
              <div class="service-detail__heding">
                <h2 class="service-detail__jp">異文化コミュニケーション</h2>
                <p class="service-detail__en u-font-italic">
                  Cross-cultural communication
                </p>
              </div>
              <p class="service-detail__text">
                急速にグローバル化が進んでおり、ビジネスの場面に限らず様々な文化的背景を持つ者同士の交流はもはや日常的な光景となりました。
                <br />
                言語や文化が異なる相手を理解することで世界が広がり、新たなビジネスチャンスに巡り会うことは少なくありません。
                <br />
                多様な価値観を尊重しながら言葉の垣根を越え、コミュニケーションの力で他者を理解しようとする「異文化コミュニケーション」はこれからの時代、さらに重要となるスキルと言えます。
                <br />
                <br />
                コミュニケーションの基本となる日本語と英語の力を鍛えつつ、アプローチする国の文化を同時に学び、スムーズなビジネス展開をサポートいたします。
              </p>
              <div class="service-detail__dl-wrapper">
                <dl class="service-detail__dl">
                  <dt class="service-detail__dt">対象</dt>
                  <dd class="service-detail__dd">
                    ビジネスの中で使える英語コニュニケーション能力が必要な方
                  </dd>
                </dl>
                <dl class="service-detail__dl">
                  <dt class="service-detail__dt">費用</dt>
                  <dd class="service-detail__dd">時間内容要相談</dd>
                </dl>
              </div>
              <a
                href="<?php echo esc_url( home_url( '/' ) ); ?>/contact"
                class="service-detail__button btn--primary btn--icon-arrow"
              >
                お申し込みはこちら
              </a>
            </div>
          </div>
        </div>
      </article>
      <article class="service-detail__items">
        <div class="service-detail__inner">
          <div id="case3" class="service-detail__item service-detail__anchor">
            <figure class="service-detail__img">
              <img
                src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-service-detail03.png"
                alt="談話中の画像"
              />
            </figure>
            <div class="service-detail__main">
              <span class="service-detail__number u-font-italic">03</span>
              <div class="service-detail__heding">
                <h2 class="service-detail__jp">ビジネス留学プログラム</h2>
                <p class="service-detail__en u-font-italic">
                  Business study abroad program
                </p>
              </div>
              <p class="service-detail__text">
                将来的に海外で働きたい方に向けた講座をご用意しております。一般的には3ヶ月〜1年の期間で基本的な英会話スキルと、海外でのビジネスマナー習得を目指します。
                <br />
                通常の語学留学では得られないビジネスの場で通用するコミュニケーションスキル習得に重点をおいておりますので、海外でビジネス展開する際に自信を持って活動することができるようになります。
                <br />
                <br />
                お申し込みいただく際に綿密なヒアリングを行い、おすすめの留学先を複数ピックアップいたします。海外ビジネス経験豊富な講師が、留学後のキャリアプラン作成までお手伝いいたします。
              </p>
              <div class="service-detail__dl-wrapper">
                <dl class="service-detail__dl">
                  <dt class="service-detail__dt">対象</dt>
                  <dd class="service-detail__dd">
                    ビジネスの中で使える英語コニュニケーション能力が必要な方
                  </dd>
                </dl>
                <dl class="service-detail__dl">
                  <dt class="service-detail__dt">費用</dt>
                  <dd class="service-detail__dd">時間内容要相談</dd>
                </dl>
              </div>
              <a
                href="<?php echo esc_url( home_url( '/' ) ); ?>/contact"
                class="service-detail__button btn--primary btn--icon-arrow"
              >
                お申し込みはこちら
              </a>
            </div>
          </div>
        </div>
      </article>
    </section>
    <!-- service-detail終了 -->
    <!-- service-flow開始 -->
    <section class="service-flow section">
      <div class="inner">
        <h2 class="service-flow__title">導入の流れ</h2>
        <article class="service-flow__items">
          <div class="service-flow__item service-flow__item--item01">
            <div class="service-flow__steps u-font-italic">
              <p class="service-flow__step">STEP</p>
              <p class="service-flow__step-number">01</p>
            </div>
            <div
              class="service-flow__main service-flow__main--main01 js-target-height"
              style="height: 210.703px;"
            >
              <h3 class="service-flow__item-title">お問い合わせ</h3>
              <p class="service-flow__item-text">
                お問い合わせフォームより必要事項をご入力の上、お申し込みください
              </p>
            </div>
          </div>
          <div class="service-flow__item service-flow__item--item02">
            <div class="service-flow__steps u-font-italic">
              <p class="service-flow__step">STEP</p>
              <p class="service-flow__step-number">02</p>
            </div>
            <div
              class="service-flow__main service-flow__main--main02 js-target-height"
              style="height: 210.703px;"
            >
              <h3 class="service-flow__item-title">ご提案</h3>
              <p class="service-flow__item-text">
                ご依頼の背景をお伺いし、必要なスキルと習得期間から最適なプランをご提案いたします
              </p>
            </div>
          </div>
          <div class="service-flow__item service-flow__item--item03">
            <div class="service-flow__steps u-font-italic">
              <p class="service-flow__step">STEP</p>
              <p class="service-flow__step-number">03</p>
            </div>
            <div
              class="service-flow__main service-flow__main--main03 js-target-height"
              style="height: 210.703px;"
            >
              <h3 class="service-flow__item-title">日程調整</h3>
              <p class="service-flow__item-text">
                研修日数と開始日を調整し、今後の流れ全体の段取りをご提案いたします
              </p>
            </div>
          </div>
          <div class="service-flow__item service-flow__item--item04">
            <div class="service-flow__steps u-font-italic">
              <p class="service-flow__step">STEP</p>
              <p class="service-flow__step-number">04</p>
            </div>
            <div
              class="service-flow__main service-flow__main--main04 js-target-height"
              style="height: 210.703px;"
            >
              <h3 class="service-flow__item-title">研修開始</h3>
              <p class="service-flow__item-text">
                研修当日はお約束のお時間の30分前に講師が伺います。
              </p>
              <p class="service-flow__item-apdx">
                ※キャンセルのご連絡は2日前までにお願いいたします
              </p>
            </div>
          </div>
        </article>
      </div>
    </section>
    <!-- service-flow終了 -->
    <!-- service-faq開始 -->
    <section class="service-faq">
      <div class="inner">
        <h2 class="service-faq__title">よくある質問</h2>
        <div class="service-faq__items accordion">
        <?php echo nl2br( esc_html( get_field( 'service' ) ) ); ?>
          <div class="accordion__item">
          <?php if ( get_field( 'question1' ) ) : ?>
            <h3 class="accordion__title js-accordion">
              <?php the_field( 'question1' ); ?>
            </h3>
            <?php endif; ?>
            <?php if ( get_field( 'answer1' ) ) : ?>
            <p class="accordion__content">
            <?php the_field( 'answer1' ); ?>
            </p>
            <?php endif; ?>
          </div>
          <div class="accordion__item">
          <?php if ( get_field( 'question2' ) ) : ?>
            <h3 class="accordion__title js-accordion">
              <?php the_field( 'question2' ); ?>
            </h3>
            <?php endif; ?>
            <?php if ( get_field( 'answer2' ) ) : ?>
            <p class="accordion__content">
            <?php the_field( 'answer2' ); ?>
            </p>
            <?php endif; ?>
          </div>
          <div class="accordion__item">
          <?php if ( get_field( 'question3' ) ) : ?>
            <h3 class="accordion__title js-accordion">
              <?php the_field( 'question3' ); ?>
            </h3>
            <?php endif; ?>
            <?php if ( get_field( 'answer3' ) ) : ?>
            <p class="accordion__content">
            <?php the_field( 'answer3' ); ?>
            </p>
            <?php endif; ?>
          </div>
          <div class="accordion__item">
          <?php if ( get_field( 'question4' ) ) : ?>
            <h3 class="accordion__title js-accordion">
              <?php the_field( 'question4' ); ?>
            </h3>
            <?php endif; ?>
            <?php if ( get_field( 'answer4' ) ) : ?>
            <p class="accordion__content">
            <?php the_field( 'answer4' ); ?>
            </p>
            <?php endif; ?>
          </div>
          <div class="accordion__item">
          <?php if ( get_field( 'question5' ) ) : ?>
            <h3 class="accordion__title js-accordion">
              <?php the_field( 'question5' ); ?>
            </h3>
            <?php endif; ?>
            <?php if ( get_field( 'answer5' ) ) : ?>
            <p class="accordion__content">
            <?php the_field( 'answer5' ); ?>
            </p>
            <?php endif; ?>
          </div>
          <div class="accordion__item">
          <?php if ( get_field( 'question6' ) ) : ?>
            <h3 class="accordion__title js-accordion">
              <?php the_field( 'question6' ); ?>
            </h3>
            <?php endif; ?>
            <?php if ( get_field( 'answer6' ) ) : ?>
            <p class="accordion__content">
            <?php the_field( 'answer6' ); ?>
            </p>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>
    <!-- service-faq終了 -->


<?php get_footer(); ?>