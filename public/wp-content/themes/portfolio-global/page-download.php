<?php get_header(); ?>

<!-- 下層ページmv -->
<div class="mv-lower">
  <div class="mv-lower__img--download">
  </div>
  <div class="mv-lower__main">
    <div class="mv-lower__en-wrapper">
      <h2 class="mv-lower__en u-font-italic">DOUNLOAD</h2>
    </div>
    <div class="mv-lower__ja-wrapper">
      <p class="mv-lower__ja">資料ダウンロード</p>
    </div>
  </div>
</div>
<!-- 下層ページmv終了 -->
<!-- パンクズ開始 -->
<div class="breadcrumbs">
  <div class="inner">
    <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
      <?php bcn_display(); //BreadcrumbNavXTのパンくずを表示するための記述 
      ?>
    </ol>
  </div>
</div>
<!-- パンクズ終了 -->
<!-- ダウンロード開始 -->
<section class="download section--lower-fv">
  <div class="inner">
    <!-- ここからflex -->
    <div class="download__items">
      <div class="download__document">
        <h2 class="download__document-title">世界で活躍できるグローバルな人材を育てる３つの研修プログラムをご用意しております。</h2>
        <div class="download__document-img"><img src="<?php echo get_template_directory_uri(); ?>/images/common/pamphlet.png" alt="ビジネス英会話研修のパンフレット"></div>
        <p class="download__document-text">
          急速にグローバルに活躍できる企業が生き残る時代と移り変わりました。<br />
          ビジネス英語や経営学を効率よく学びながら、世界各国から集まるビジネスパーソンと交流し、世界レベルでの人脈を構築する研修をご用意しております。<br /><br />
          英語に苦手意識のある方でもご安心ください。<br />
          ビジネスで必要なコミュニケーションが取れるようになるまで実績豊富な講師陣がサポートいたします。<br />
          まずはこちらの資料をごらんください。
        </p>
      </div>
      <div class="download__form">
        <h2 class="download__form-title">資料ダウンロード</h2>
        <div class="download__form-items sub-contact__contents">
          <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
              <?php the_content(); ?>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
      <!-- ここまでflex -->
    </div>
</section>

<?php get_footer(); ?>