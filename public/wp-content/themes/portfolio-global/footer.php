<!-- footer開始 -->
<div class="footer-links">
      <div class="footer-links__item--download">
        <div class="footer-links__main">
          <h2 class="footer-links__en u-font-italic">DOWNLOAD</h2>
          <p class="footer-links__ja">資料ダウンロード</p>
          <a
            href="<?php echo esc_url( home_url( '/' ) ); ?>/download"
            class="footer-links__button btn--tertiary btn--icon-arrow u-font-italic"
          >
            View more
          </a>
        </div>
      </div>
      <div class="footer-links__item--contact">
        <div class="footer-links__main">
          <h2 class="footer-links__en u-font-italic">CONTACT</h2>
          <p class="footer-links__ja">お問い合わせ</p>
          <a
            href="<?php echo esc_url( home_url( '/' ) ); ?>/contact"
            class="footer-links__button btn--tertiary btn--icon-arrow u-font-italic"
          >
            View more
          </a>
        </div>
      </div>
    </div>

    <footer id="footer" class="footer">
      <div class="inner">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="footer__logo">
          <img src="<?php echo get_template_directory_uri( ); ?>/images/common/logo02.png" alt="GlobalStandard" />
        </a>
        <div class="footer__info">
          <p class="footer__info-text">
            〒550-1000　大阪市西区土佐堀9-5-5
            <br />
            TEL　06-123-4567
            <br />
            FAX　06-123-4568
          </p>
        </div>
        <p class="footer__copyright">
          &copy;2021 Global Standard. All Rights Reserved.
        </p>
      </div>
      <div id="js-pagetop" class="pagetop">
        <img src="<?php echo get_template_directory_uri( ); ?>/images/common/icon-top.png" alt="↑" />
      </div>
    </footer>
    <!-- フッター終了 -->
    <?php wp_footer(); ?>
  </body>
</html>
