<?php get_header(); ?>

<!-- 下層ページmv -->
<div class="mv-lower">
  <div class="mv-lower__img--contact">
  </div>
  <div class="mv-lower__main">
    <div class="mv-lower__en-wrapper">
      <h2 class="mv-lower__en u-font-italic">CONTACT</h2>
    </div>
    <div class="mv-lower__ja-wrapper">
      <p class="mv-lower__ja">お問い合わせ</p>
    </div>
  </div>
</div>
<!-- 下層ページmv終了 -->
<!-- パンクズ開始 -->
<div class="breadcrumbs">
  <div class="inner">
    <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
      <?php bcn_display(); //BreadcrumbNavXTのパンくずを表示するための記述 
      ?>
    </ol>
  </div>
</div>
<!-- パンクズ終了 -->
<section class="main-contact section--lower-fv">
  <div class="inner">
    <div class="main-contact__item">
      <p class="main-contact__intro">
        研修のお申し込み、その他お問い合わせは、下記のフォームからお問い合わせ内容をご記入ください。<br />
        2日以内に担当者からメールにてご連絡いたします。
      </p>
      <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
          <?php the_content(); ?>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>