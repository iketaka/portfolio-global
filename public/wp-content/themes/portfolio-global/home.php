<?php get_header(); ?>

<!-- 下層ページmv -->
<div class="mv-lower">
  <div class="mv-lower__img--news"></div>
  <div class="mv-lower__main">
    <div class="mv-lower__en-wrapper">
      <h2 class="mv-lower__en u-font-italic">NEWS</h2>
    </div>
    <div class="mv-lower__ja-wrapper">
      <p class="mv-lower__ja">ニュース</p>
    </div>
  </div>
</div>
<!-- 下層ページmv終了 -->
<!-- パンクズ開始 -->
<div class="breadcrumbs">
  <div class="inner">
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
      <?php if (function_exists('bcn_display')) {
        bcn_display();
      } ?>
    </div>
  </div>
</div>
<!-- パンクズ終了 -->
<!-- コンテンツ開始 -->
<section class="contnts section--lower-fv">
  <div class="inner">
    <div class="contents__inner">
      <main class="archive-news contents__main">
        <h2 class="archive-news__cat-name"><?php the_archive_title(); //一覧ページ名を表示 
                                            ?></h2>
        <!-- ニュースパーツ開始 -->
        <?php
        //記事があればentriesブロック以下を表示
        if (have_posts()) :
        ?>
          <?php
          //記事数ぶんループ
          while (have_posts()) :
            the_post();
          ?>
            <a href="<?php the_permalink(); //記事のリンクを表示 
                      ?>" class="archive-news__item">
              <div class="archive-news__thumnail">
                <?php
                if (has_post_thumbnail()) {
                  // アイキャッチ画像が設定されてれば大サイズで表示
                  the_post_thumbnail('large');
                } else {
                  // なければnoimage画像をデフォルトで表示
                  echo '<img src="' . esc_url(get_template_directory_uri()) . '/images/common/single.png" alt="">';
                }
                ?>
              </div>
              <div class="archive-news__body">
                <div class="meta archive-news__meta">
                  <div class="meta__cats archive-news__cats">
                    <?php
                    // カテゴリー１つ目の名前を表示
                    $category = get_the_category();
                    if ($category[0]) {
                      echo '<span class="meta__cat">' . $category[0]->cat_name . '</span>';
                    }
                    ?>
                    <?php
                    $span = 3;  // NEWマークを表示する期間（このコードでは日間）
                    $now = date_i18n('U');  // 現在の時間
                    $entry = get_the_time('U');  // 投稿日の時間
                    $term = date('U', ($now - $entry)) / 86400;
                    if ($span > $term) {
                      echo '<span class="meta__new">NEW</span>';
                    }
                    ?>
                  </div>
                  <time class="meta__time"><?php the_time('Y/n/j'); ?></time>
                </div>
                <h2 class="archive-news__title"><?php the_title(); ?></h2>
              </div>
            </a>
          <?php
          endwhile;
          ?>
        <?php endif; ?>
        <!-- ニュースパーツ終了 -->
        <!-- ページネーション開始 -->
        <div class="pagination archive-news__pagination">
          <?php wp_pagenavi(); ?>
        </div>
        <!-- ページネーション終了 -->
      </main>
      <!-- サイドバー開始 -->
      <aside class="sidebar contents__side">
        <article class="sidebar-new">
          <h2 class="sidebar__title">新着記事</h2>
          <!-- サイドバーパーツ開始 -->
          <?php
          $pickup_posts = get_posts(array(
            'post_type' => 'post', // 投稿タイプ
            'posts_per_page' => '5', // 5件取得
            'tag' => 'pickup', // pickupタグがついたものを
            'orderby' => 'DESC', // 新しい順に
          ));
          ?>
          <?php foreach ($pickup_posts as $post) : setup_postdata($post); ?>
            <a href="<?php echo esc_url(get_permalink()); ?>" class="sidebar-new__item">
              <div class="sidebar-new__thumnail">
                <img src="<?php echo get_template_directory_uri(); ?>/images/common/single.png" alt="" />
              </div>
              <div class="sidebar-new__body">
                <div class="sidebar-new__meta">
                  <?php
                  // カテゴリー１つ目の名前を表示
                  $category = get_the_category();
                  if ($category[0]) {
                    echo '<span class="sidebar-new__cat">' . $category[0]->cat_name . '</span>';
                  }
                  ?>
                  <time class="sidebar-new__time" datetime="<?php the_time('c'); ?>"><?php the_time('Y/n/j'); ?></time>
                </div>
                <h2 class="sidebar-new__title"><?php the_title(); ?></h2>
              </div>
            </a>
          <?php endforeach;
          wp_reset_postdata(); ?>
          <!-- サイドバーパーツ終了 -->
        </article>
        <article class="sidebar-cats">
          <h2 class="sidebar__title">カテゴリ</h2>
          <ul class="sidebar-cats__lists">
            <?php
            $args = array();
            $categories = get_categories($args);
            foreach ($categories as $category) {
              echo '<div class="sidebar-cats__list">
    <a href="' . get_category_link($category->term_id) . '" class="sidebar-cats__link">' . $category->name . '</a>
  </div>';
            }
            ?>
          </ul>
        </article>
      </aside>
    </div>
  </div>
</section>


<?php get_footer(); ?>