<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="format-detection" content="telephone=no" />
  <!-- meta情報 -->
  <title>GlobalStandard</title>
  <meta name='robots' content='noindex, nofollow' />
  <meta name="description" content="仮想上のコーポレイトサイトです。急速に広がったグローバル社会に対応できる人材を育成することで、文化・言語の垣根を越えたコミュニケーションを活発にし、一人でも多くの人が豊かに暮らせる世界を実現することを使命とした企業です" />
  <!-- ogp -->
  <meta property="og:title" content="GlobalStandard" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="" />
  <meta property="og:image" content="" />
  <meta property="og:site_name" content="GlobalStandard" />
  <meta property="og:description" content="仮想上のコーポレイトサイトです。急速に広がったグローバル社会に対応できる人材を育成することで、文化・言語の垣根を越えたコミュニケーションを活発にし、一人でも多くの人が豊かに暮らせる世界を実現することを使命とした企業です" />
  <!-- css -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/styles.css" />

  <!-- swiper -->
  <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />
  <!-- google font -->
  <link rel='stylesheet' id='google-fonts-noto-sans-css'  href='https://fonts.googleapis.com/css2?family=Noto+Sans+JP%3Awght%40500%3B700&#038;display=swap%22+rel%3D%22stylesheet%22+rel%3D%22stylesheet%22&#038;ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='google-fonts-fira-sans-css'  href='https://fonts.googleapis.com/css2?family=Fira+Sans%3Aital%2Cwght%401%2C500&#038;display=swap%22+rel%3D%22stylesheet%22+rel%3D%22stylesheet%22&#038;ver=1.0.0' type='text/css' media='all' />
  <!-- swiper -->
  <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
  <!-- JavaScript -->
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.4/ofi.js"></script>
  <?php wp_head(); ?>
</head>

<body>
  <!-- header -->
  <div class="header--spacing js-drawer-open"></div>
  <header id="header" class="header js-drawer-open">
    <div class="header__inner">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="header__logo">
        <h1 class="header__title">
          <img src="<?php echo get_template_directory_uri(); ?>/images/common/logo01.png" alt="ロゴの画像" />
        </h1>
      </a>

      <!-- <?php
      $defaults = array(
        'menu_class'      => 'gnav__lists',
        'container'       => 'nav',
        'container_class' => 'gnav',
        'depth'           => 1,
        'theme_location'  => 'main',
        'add_li_class' => 'gnav__list', // liタグへclass追加
        'add_a_class' => 'gnav__link text-white' // aタグへclass追加
      );
      wp_nav_menu($defaults);
      ?>

 -->

      <nav class="gnav">
        <ul class="gnav__lists">
          <li class="gnav__list">
            <a class="gnav__link" href="<?php echo esc_url( home_url( '/' ) ); ?>">トップ</a>
          </li>
          <li class="gnav__list">
            <a class="gnav__link" href="<?php echo esc_url( home_url( '/' ) ); ?>/company">当社について</a>
          </li>
          <li class="gnav__list">
            <a class="gnav__link" href="<?php echo esc_url( home_url( '/' ) ); ?>/service">サービス</a>
          </li>
          <li class="gnav__list">
            <a class="gnav__link" href="<?php echo esc_url( home_url( '/' ) ); ?>/case">導入事例</a>
          </li>
          <li class="gnav__list">
            <a class="gnav__link" href="<?php echo esc_url( home_url( '/' ) ); ?>/news">お知らせ</a>
          </li>
        </ul>
        <div class="gnav__buttons">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>/download" class="btn--primary gnav__button">
            資料ダウンロード
          </a>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>/contact" class="btn--secondary gnav__button">
            お問い合わせ
          </a>
        </div>

      </nav>
      <!-- ドロワーメニュー -->
      <div class="drawer">
        <div class="drawer__icon-area">
          <input id="drawer-checkbox" class="drawer__checkbox" type="checkbox" />
          <label id="drawer-icon" for="drawer-checkbox" class="drawer__icon">
            <span class="drawer__bar"></span>
          </label>
          <label id="drawer-close" for="drawer-checkbox"></label>
        </div>
        <div id="drawer-content" class="drawer__content">
          <div class="drawer__inner">
            <nav class="drawer__nav">
              <ul class="drawer__lists">
                <li class="drawer__list">
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="drawer__link">
                    トップ
                  </a>
                </li>
                <li class="drawer__list">
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>/company" class="drawer__link">当社について</a>
                </li>
                <li class="drawer__list">
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>/service" class="drawer__link">サービス</a>
                </li>
                <li class="drawer__list">
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>/case" class="drawer__link">導入事例</a>
                </li>
                <li class="drawer__list">
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>/news" class="drawer__link">お知らせ</a>
                </li>
              </ul>
            </nav>
            <div class="drawer__buttons">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>/download" class="drawer__button--download">
                資料ダウンロード
              </a>
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>/contact" class="drawer__button--contact">
                お問い合わせ
              </a>
            </div>
          </div>
        </div>
      </div>
      <!-- ドロワー終了 -->
    </div>
  </header>