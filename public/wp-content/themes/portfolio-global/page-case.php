<?php get_header(); ?>

<!-- 下層ページmv -->
    <div class="mv-lower">
      <div class="mv-lower__img--case">
      </div>
      <div class="mv-lower__main">
        <div class="mv-lower__en-wrapper">
          <h2 class="mv-lower__en u-font-italic">CASE STUDY</h2>
        </div>
        <div class="mv-lower__ja-wrapper">
          <p class="mv-lower__ja">導入事例</p>
        </div>
      </div>
    </div>
    <!-- 下層ページmv終了 -->
    <!-- パンクズ開始 -->
<div class="breadcrumbs">
  <div class="inner">
    <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
      <?php bcn_display(); //BreadcrumbNavXTのパンくずを表示するための記述 ?>
    </ol>
  </div>
</div>
<!-- パンクズ終了 -->
    <!-- case-study開始 -->
    <section class="case-study section--lower-fv">
      <div class="inner">
        <nav class="case-study__nav">
          <ul class="case-study__nav-lists">
            <li class="case-study__nav-list">
              <a href="#a-biz" class="case-study__nav-link">ビジネス英語研修</a>
            </li>
            <li class="case-study__nav-list">
              <a href="#a-cross" class="case-study__nav-link">異文化コミュニケーション</a>
            </li>
            <li class="case-study__nav-list">
              <a href="#a-abroad" class="case-study__nav-link">ビジネス留学プログラム</a>
            </li>
          </ul>
        </nav>
    <!-- case-study終了 -->
    <!-- cases1開始 -->
    <section id="a-biz" class="cases">
      <h2 class="cases__title">
        ビジネス英語研修
        <span class="cases__title--en u-font-italic">Business English Training</span>
      </h2>
      <article class="cases__items">
        <div id="AAA" class="cases__item">
          <div class="cases__item-head">
            <div class="cases__item-heading">
              <p class="cases__business">ソフトウェア開発事業</p>
              <p class="cases__name">AAA株式会社<span class="cases__compellation">様</span></p>
            </div>
            <div class="cases__item-logo">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-1.png" alt="">
            </div>
          </div>
          <div class="cases__item-body">
            <h3 class="cases__item-title">研修コース：
              <span class="cases__item-title--course-name">ビジネス研修英語</span>
            </h3>
            <ul class="cases__lists">
              <li class="cases__list">
                <h4 class="cases__list-title">
                  研修の目的
                </h4>
                <p class="cases__list-text">
                  海外にビジネス展開する計画があり、必要な英会話コミュニケーションを身につけることが目的。
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  選んだ理由
                </h4>
                <p class="cases__list-text">
                  海外の企業で働いていた実績のある講師に直接ご指導いただけることは、とても貴重な経験になると考えた為
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  導入後の成果・効果
                </h4>
                <p class="cases__list-text">
                  昨年からオーストラリアの企業と業務提携を開始し、３ヶ月という短い研修期間でしたが、業務に必要なコミュニケーションはスムーズに行えております。<br>
                  今後は更なるスキルアップを目指していきます。
                </p>
              </li>
            </ul>
          </div>
        </div>
        <div id="FFF" class="cases__item">
          <div class="cases__item-head">
            <div class="cases__item-heading">
              <p class="cases__business">Webマーケティング事業</p>
              <p class="cases__name">FFF株式会社<span class="cases__compellation">様</span></p>
            </div>
            <div class="cases__item-logo">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-2.png" alt="">
            </div>
          </div>
          <div class="cases__item-body">
            <h3 class="cases__item-title">研修コース：
              <span class="cases__item-title--course-name">ビジネス研修英語</span>
            </h3>
            <ul class="cases__lists">
              <li class="cases__list">
                <h4 class="cases__list-title">
                  研修の目的
                </h4>
                <p class="cases__list-text">
                  海外にビジネス展開する計画があり、必要な英会話コミュニケーションを身につけることが目的。
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  選んだ理由
                </h4>
                <p class="cases__list-text">
                  海外の企業で働いていた実績のある講師に直接ご指導いただけることは、とても貴重な経験になると考えた為
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  導入後の成果・効果
                </h4>
                <p class="cases__list-text">
                  昨年からオーストラリアの企業と業務提携を開始し、３ヶ月という短い研修期間でしたが、業務に必要なコミュニケーションはスムーズに行えております。<br>
                  今後は更なるスキルアップを目指していきます。
                </p>
              </li>
            </ul>
          </div>
        </div>
        <div id="GGG" class="cases__item">
          <div class="cases__item-head">
            <div class="cases__item-heading">
              <p class="cases__business">飲食事業</p>
              <p class="cases__name">株式会社GGG<span class="cases__compellation">様</span></p>
            </div>
            <div class="cases__item-logo">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-3.png" alt="">
            </div>
          </div>
          <div class="cases__item-body">
            <h3 class="cases__item-title">研修コース：
              <span class="cases__item-title--course-name">ビジネス研修英語</span>
            </h3>
            <ul class="cases__lists">
              <li class="cases__list">
                <h4 class="cases__list-title">
                  研修の目的
                </h4>
                <p class="cases__list-text">
                  海外にビジネス展開する計画があり、必要な英会話コミュニケーションを身につけることが目的。
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  選んだ理由
                </h4>
                <p class="cases__list-text">
                  海外の企業で働いていた実績のある講師に直接ご指導いただけることは、とても貴重な経験になると考えた為
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  導入後の成果・効果
                </h4>
                <p class="cases__list-text">
                  昨年からオーストラリアの企業と業務提携を開始し、３ヶ月という短い研修期間でしたが、業務に必要なコミュニケーションはスムーズに行えております。<br>
                  今後は更なるスキルアップを目指していきます。
                </p>
              </li>
            </ul>
          </div>
        </div>
        <div id="HHH" class="cases__item">
          <div class="cases__item-head">
            <div class="cases__item-heading">
              <p class="cases__business">メディカルフィットネス事業</p>
              <p class="cases__name">HHH株式会社<span class="cases__compellation">様</span></p>
            </div>
            <div class="cases__item-logo">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-4.png" alt="">
            </div>
          </div>
          <div class="cases__item-body">
            <h3 class="cases__item-title">研修コース：
              <span class="cases__item-title--course-name">ビジネス研修英語</span>
            </h3>
            <ul class="cases__lists">
              <li class="cases__list">
                <h4 class="cases__list-title">
                  研修の目的
                </h4>
                <p class="cases__list-text">
                  海外にビジネス展開する計画があり、必要な英会話コミュニケーションを身につけることが目的。
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  選んだ理由
                </h4>
                <p class="cases__list-text">
                  海外の企業で働いていた実績のある講師に直接ご指導いただけることは、とても貴重な経験になると考えた為
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  導入後の成果・効果
                </h4>
                <p class="cases__list-text">
                  昨年からオーストラリアの企業と業務提携を開始し、３ヶ月という短い研修期間でしたが、業務に必要なコミュニケーションはスムーズに行えております。<br>
                  今後は更なるスキルアップを目指していきます。
                </p>
              </li>
            </ul>
          </div>
        </div>
      </article>
      <div class="cases__button-wrapper">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>/service#case1" class="btn--primary btn--icon-arrow cases__button">ビジネス研修英語の詳細</a>
      </div>
    </section>
    <!-- cases1終了 -->
    <!-- cases2開始 -->
    <section id="a-cross" class="cases section--only-above">
      <h2 class="cases__title">
        異文化コミュニケーション
        <span class="cases__title--en u-font-italic">Cross-cultural communication</span>
      </h2>
      <article class="cases__items">
        <div id="BBB" class="cases__item">
          <div class="cases__item-head">
            <div class="cases__item-heading">
              <p class="cases__business">スポーツ用品開発事業</p>
              <p class="cases__name">合同会社BBB<span class="cases__compellation">様</span></p>
            </div>
            <div class="cases__item-logo">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-5.png" alt="">
            </div>
          </div>
          <div class="cases__item-body">
            <h3 class="cases__item-title">研修コース：
              <span class="cases__item-title--course-name">異文化コミュニケーション</span>
            </h3>
            <ul class="cases__lists">
              <li class="cases__list">
                <h4 class="cases__list-title">
                  研修の目的
                </h4>
                <p class="cases__list-text">
                  海外にビジネス展開する計画があり、必要な英会話コミュニケーションを身につけることが目的。
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  選んだ理由
                </h4>
                <p class="cases__list-text">
                  海外の企業で働いていた実績のある講師に直接ご指導いただけることは、とても貴重な経験になると考えた為
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  導入後の成果・効果
                </h4>
                <p class="cases__list-text">
                  昨年からオーストラリアの企業と業務提携を開始し、３ヶ月という短い研修期間でしたが、業務に必要なコミュニケーションはスムーズに行えております。<br>
                  今後は更なるスキルアップを目指していきます。
                </p>
              </li>
            </ul>
          </div>
        </div>
        <div id="DDD" class="cases__item">
          <div class="cases__item-head">
            <div class="cases__item-heading">
              <p class="cases__business">アパレル事業</p>
              <p class="cases__name">DDD株式会社<span class="cases__compellation">様</span></p>
            </div>
            <div class="cases__item-logo">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-6.png" alt="">
            </div>
          </div>
          <div class="cases__item-body">
            <h3 class="cases__item-title">研修コース：
              <span class="cases__item-title--course-name">異文化コミュニケーション</span>
            </h3>
            <ul class="cases__lists">
              <li class="cases__list">
                <h4 class="cases__list-title">
                  研修の目的
                </h4>
                <p class="cases__list-text">
                  海外にビジネス展開する計画があり、必要な英会話コミュニケーションを身につけることが目的。
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  選んだ理由
                </h4>
                <p class="cases__list-text">
                  海外の企業で働いていた実績のある講師に直接ご指導いただけることは、とても貴重な経験になると考えた為
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  導入後の成果・効果
                </h4>
                <p class="cases__list-text">
                  昨年からオーストラリアの企業と業務提携を開始し、３ヶ月という短い研修期間でしたが、業務に必要なコミュニケーションはスムーズに行えております。<br>
                  今後は更なるスキルアップを目指していきます。
                </p>
              </li>
            </ul>
          </div>
        </div>
        <div id="Ⅲ" class="cases__item">
          <div class="cases__item-head">
            <div class="cases__item-heading">
              <p class="cases__business">ソフトウェア開発事業</p>
              <p class="cases__name">Ⅲ株式会社<span class="cases__compellation">様</span></p>
            </div>
            <div class="cases__item-logo">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-7.png" alt="">
            </div>
          </div>
          <div class="cases__item-body">
            <h3 class="cases__item-title">研修コース：
              <span class="cases__item-title--course-name">異文化コニュニケーション</span>
            </h3>
            <ul class="cases__lists">
              <li class="cases__list">
                <h4 class="cases__list-title">
                  研修の目的
                </h4>
                <p class="cases__list-text">
                  海外にビジネス展開する計画があり、必要な英会話コミュニケーションを身につけることが目的。
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  選んだ理由
                </h4>
                <p class="cases__list-text">
                  海外の企業で働いていた実績のある講師に直接ご指導いただけることは、とても貴重な経験になると考えた為
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  導入後の成果・効果
                </h4>
                <p class="cases__list-text">
                  昨年からオーストラリアの企業と業務提携を開始し、３ヶ月という短い研修期間でしたが、業務に必要なコミュニケーションはスムーズに行えております。<br>
                  今後は更なるスキルアップを目指していきます。
                </p>
              </li>
            </ul>
          </div>
        </div>
      </article>
      <div class="cases__button-wrapper">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>/service#case2" class="btn--primary btn--icon-arrow cases__button">異文化コミュニケーションの詳細</a>
      </div>
    </section>
    <!-- cases2終了 -->
    <!-- cases3開始 -->
    <section id="a-abroad" class="cases section--only-above">
      <h2 class="cases__title">
        ビジネス留学プログラム
        <span class="cases__title--en u-font-italic">Business study abroad program</span>
      </h2>
      <article class="cases__items">
        <div id="CCC" class="cases__item">
          <div class="cases__item-head">
            <div class="cases__item-heading">
              <p class="cases__business">マーケティング事業</p>
              <p class="cases__name">株式会社CCC<span class="cases__compellation">様</span></p>
            </div>
            <div class="cases__item-logo">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-8.png" alt="">
            </div>
          </div>
          <div class="cases__item-body">
            <h3 class="cases__item-title">研修コース：
              <span class="cases__item-title--course-name">ビジネス留学プログラム</span>
            </h3>
            <ul class="cases__lists">
              <li class="cases__list">
                <h4 class="cases__list-title">
                  研修の目的
                </h4>
                <p class="cases__list-text">
                  海外にビジネス展開する計画があり、必要な英会話コミュニケーションを身につけることが目的。
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  選んだ理由
                </h4>
                <p class="cases__list-text">
                  海外の企業で働いていた実績のある講師に直接ご指導いただけることは、とても貴重な経験になると考えた為
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  導入後の成果・効果
                </h4>
                <p class="cases__list-text">
                  昨年からオーストラリアの企業と業務提携を開始し、３ヶ月という短い研修期間でしたが、業務に必要なコミュニケーションはスムーズに行えております。<br>
                  今後は更なるスキルアップを目指していきます。
                </p>
              </li>
            </ul>
          </div>
        </div>
        <div id="EEE" class="cases__item">
          <div class="cases__item-head">
            <div class="cases__item-heading">
              <p class="cases__business">国内旅行事業</p>
              <p class="cases__name">EEE株式会社<span class="cases__compellation">様</span></p>
            </div>
            <div class="cases__item-logo">
              <img src="<?php echo get_template_directory_uri(  ); ?>/images/common/img-case-9.png" alt="">
            </div>
          </div>
          <div class="cases__item-body">
            <h3 class="cases__item-title">研修コース：
              <span class="cases__item-title--course-name">ビジネス留学プログラム</span>
            </h3>
            <ul class="cases__lists">
              <li class="cases__list">
                <h4 class="cases__list-title">
                  研修の目的
                </h4>
                <p class="cases__list-text">
                  海外にビジネス展開する計画があり、必要な英会話コミュニケーションを身につけることが目的。
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  選んだ理由
                </h4>
                <p class="cases__list-text">
                  海外の企業で働いていた実績のある講師に直接ご指導いただけることは、とても貴重な経験になると考えた為
                </p>
              </li>
              <li class="cases__list">
                <h4 class="cases__list-title">
                  導入後の成果・効果
                </h4>
                <p class="cases__list-text">
                  昨年からオーストラリアの企業と業務提携を開始し、３ヶ月という短い研修期間でしたが、業務に必要なコミュニケーションはスムーズに行えております。<br>
                  今後は更なるスキルアップを目指していきます。
                </p>
              </li>
            </ul>
          </div>
        </div>
      </article>
      <div class="cases__button-wrapper">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>/service#case3" class="btn--primary btn--icon-arrow cases__button">ビジネス留学プログラムの詳細</a>
      </div>
    </section>
    <!-- cases3終了 -->
    </div>
</section>

<?php get_footer(); ?>